mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(dir $(mkfile_path))

DOCKER := $(shell which docker)
DOCKER_IMAGE ?= "technep/yocto:latest"

CONTAINER_WORK_DIR ?= "/home/developer/yocto"
MANIFEST_URI ?= "https://gitlab.com/technep/yocto/yocto-manifest.git"
MANIFEST_TAG ?= "dunfell"

KAS_YAML_URI ?= "https://gitlab.com/technep/yocto/meta-technep/-/raw/master/kas-technep-rpi0w.yml"

MACHINES := $(shell ls $(current_dir)sources/**/conf/machine/*.conf)

IMAGES :=  $(shell ls $(current_dir)sources/**/meta-*/recipes*/images/*.bb | sed -n '1d;p')

DOCKER_RUN_ARGS += --env DL_DIR=${CONTAINER_WORK_DIR}/output/build/downloads
DOCKER_RUN_ARGS += --env SSTATE_DIR=${CONTAINER_WORK_DIR}/output/build/sstate-cache
DOCKER_RUN_ARGS += --env TMPDIR=${CONTAINER_WORK_DIR}/output/build/tmp
DOCKER_RUN_ARGS += -v $(current_dir)sources:$(CONTAINER_WORK_DIR)/sources
DOCKER_RUN_ARGS += -v $(current_dir)output:$(CONTAINER_WORK_DIR)/output


run:
	@echo "MAKEFILE DIRECTORY $(current_dir)"
	@echo "CONTAINER WORKING DIR $(CONTAINER_WORK_DIR)"
	$(DOCKER) run -it --rm  \
        --env "DL_DIR=${CONTAINER_WORK_DIR}/output/build/downloads" \
        --env "SSTATE_DIR=${CONTAINER_WORK_DIR}/output/build/sstate-cache" \
        --env "TMPDIR=${YOCTO_TMP_DIR}/output/build/tmp" \
	-v $(current_dir)sources:$(CONTAINER_WORK_DIR)/sources \
	-v $(current_dir)output:$(CONTAINER_WORK_DIR)/output $(DOCKER_IMAGE) bash

docker_build:
	cd "$(current_dir) && \
	$(DOCKER)  build   \
	--build-arg "host_uid=$(shell id -u)"   \
	--build-arg "host_gid=$(shell id -g)"   \
	--build-arg "yocto_manifest_uri=$(MANIFEST_URI)"   \
	--build-arg "yocto_manifest_tag=$(MANIFEST_TAG)"   \
	--tag "$(DOCKER_IMAGE)" .

run_ubuntu:
	$(DOCKER) run -it --rm -v $(current_dir)sources:/home/dev/sources -v $(current_dir)output:/home/dev/output yocto-compile:latest bash 

available_images:
	@echo "Available Images"
	ls $(current_dir)sources/**/conf/machine/*.conf

available_machines:
	@echo "Available Machines"
	ls $(current_dir)sources/**/meta-*/recipes*/images/*.bb

printnow:
	@echo $(DOCKER_RUN_ARGS)

kas-build:
	@echo "MAKEFILE DIRECTORY $(current_dir)"
	$(DOCKER) run $(DOCKER_RUN_ARGS) $(DOCKER_IMAGE) yocto-build $(KAS_YAML_URI)

kas-shell:
	@echo "MAKEFILE DIRECTORY $(current_dir)"
	$(DOCKER) run -it --rm $(DOCKER_RUN_ARGS) $(DOCKER_IMAGE) yocto-shell


