#!/bin/bash
set -e

print_usage()
{
	echo "Usage: command args"
	echo "---------------------"
	echo "yocto-build <uri> | yocto-build <file.yml>"
	echo "   eg. yocto-build 'https://gitlab.com/technep/yocto/meta-technep/-/raw/master/kas-technep-rpi0w.yml'"
    echo "yocto-build"
    echo "   only if yocto-build <uri> run before"
    echo "yocto-shell <uri> | yocto-shell <uri> <args>"
    echo "   get a shell with sourced poky variables"
	echo "yocto-pull <uri>"
	echo "   Set up layers without actual build"
	echo "   To build run yocto-build" 
	echo "bash args"
	echo "   Get bash"
	echo "help"
	echo "   This menu will be printed"
	echo "<command> <args>"
	echo "   Run any commant"
}

KAS_SYM_YML="${KAS_WORK_DIR}/kas-last-used.yml"

download_yml(){
	if curl -k --output /dev/null --silent --head --fail "$1" ; then
        curl -k --silent -O "$1" && ln -sf "${KAS_WORK_DIR}/${1##*/}" "${KAS_SYM_YML}"
    elif [ -f "${KAS_WORK_DIR}/$1" ] ; then
		[ ! -h "${KAS_WORK_DIR}/$1" ] && ln -sf "${KAS_WORK_DIR}/$1" "${KAS_SYM_YML}"
	else
        echo "Url Failed or No arg passed: $1"
        return 1
    fi
	return 0
}


main()
{
	if [ "${BUILD_YAML_URI}" != "none" ] && [ ! -f "${KAS_SYM_YML}" ] ; then
		if curl -k --output /dev/null --silent --head --fail "${BUILD_YAML_URI}" ; then
			curl -k --silent -O "$1" && ln -sf "${KAS_WORK_DIR}/${1##*/}" "${KAS_SYM_YML}"
		fi
	fi
	
	case "$1" in
		yocto-build)
			shift; set -- "$@"
			if [ $# -eq 0 ] ; then 
				if [ ! -f "${KAS_SYM_YML}" ] ; then
					echo "Run yocto-init <uri>"
					exit 1
				else
					kas build "${KAS_SYM_YML}"
				fi
			else
				download_yml "$1" && kas build "${KAS_SYM_YML}"
			fi
		;;
		yocto-shell)
            shift; set -- "$@"
                if [ $# -eq 0 ] ; then
					if [ ! -f "${KAS_SYM_YML}" ] ; then
                        echo "Run yocto-init <uri>"
						exit 1
					else
						kas shell "${KAS_SYM_YML}"
					fi
                else 
					uri="$1"; shift; set -- "$@"
					if [ $# -eq 0 ] ; then 
						download_yml "$uri" && kas shell "${KAS_SYM_YML}" 
					else 
						download_yml "$uri" && kas shell "${KAS_SYM_YML}" -c "$@"
					fi
                fi
		;;
        yocto-pull)
            shift; set -- "$@"
                if [ $# -eq 0 ] && [ ! -f "${KAS_SYM_YML}" ] ; then
                    echo "Run yocto-pull <uri>"
                    exit 1
                else 
                    download_yml "$1" && kas shell "${KAS_SYM_YML}" -c 'exit'
                fi
		;;
		bash)
			shift; set -- "$@"
			bash "$@"
		;;
		help)
			print_usage
			exit 0
		;;
		*)
			exec "$@"

	esac
}

main $@
