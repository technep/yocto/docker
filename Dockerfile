FROM debian:buster-slim

# How to build docker image
# docker build 
#  --no-cache 
#  --build-arg "host_uid=$(id -u)" \
#  --build-arg "host_gid=$(id -g)" \
#  --tag "project-yocto-image:latest" .

RUN  mkdir -p /etc/dpkg/dpkg.conf.d && \
    touch /etc/dpkg/dpkg.conf.d/01_nodoc && \
    echo "path-exclude=/usr/share/man/* " >  "/etc/dpkg/dpkg.conf.d/01_nodoc" && \
    echo "path-exclude=/usr/share/doc/* " >>  "/etc/dpkg/dpkg.conf.d/01_nodoc" && \
    echo "path-include=/usr/share/doc/*/copyright" >> "/etc/dpkg/dpkg.conf.d/01_nodoc" && \
    echo "APT::Install-Recommends \"0\";" > "/etc/apt/apt.conf.d/01norecommend" && \
    echo "APT::Install-Suggests \"0\";" >>  "/etc/apt/apt.conf.d/01norecommend" && \
	apt-get update && \
    apt-get -y install \
    gawk wget git-core diffstat unzip texinfo gcc-multilib \
    build-essential chrpath socat cpio python3 python3-pip python3-pexpect \
    xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev \
    pylint3 xterm \
    file curl tar locales sudo vim bash-completion screen nano && \
    rm -rf /var/lib/apt/lists/*

RUN rm /bin/sh && ln -sf /bin/bash /bin/sh && \
    localedef -i en_US -c -f UTF-8 -A \
    /usr/share/locale/locale.alias en_US.UTF-8

ENV LANG en_US.utf8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8


ENV USER_NAME developer
ENV PROJECT customyocto

ARG host_uid=1001
ARG host_gid=1001
RUN groupadd -g $host_gid $USER_NAME && \
    useradd -g $host_gid -m -s /bin/bash -u $host_uid $USER_NAME && \
    echo "$USER_NAME ALL=(ALL) NOPASSWD:ALL" >>  /etc/sudoers

USER $USER_NAME

ENV BUILD_SOURCE_DIR /home/$USER_NAME/yocto/sources
ENV BUILD_OUTPUT_DIR /home/$USER_NAME/yocto/output

RUN mkdir -p $BUILD_SOURCE_DIR $BUILD_OUTPUT_DIR

USER root

RUN python3 -m pip install --no-cache-dir \
    setuptools wheel pyaml && \
    python3 -m pip install --no-cache-dir kas

USER $USER_NAME
WORKDIR $BUILD_SOURCE_DIR

ARG kas_yaml_uri="none"


ENV BUILD_YAML_URI "$kas_yaml_uri"

ARG YOCTO_SOURCE_DIR="/home/$USER_NAME/yocto/sources"
ENV YOCTO_SOURCE_DIR $YOCTO_SOURCE_DIR

ENV KAS_WORK_DIR $YOCTO_SOURCE_DIR

WORKDIR ${KAS_WORK_DIR}

USER root

RUN curl -k https://gitlab.com/technep/yocto/docker/-/raw/master/entrypoint.sh > /docker-entrypoint.sh && \
    chmod a+x /docker-entrypoint.sh
    
USER $USER_NAME

ENTRYPOINT ["/docker-entrypoint.sh"]


